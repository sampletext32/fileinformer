﻿using System;
using FileInformer;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Tests
{
    [TestClass]
    public class AnalyzerTests
    {
        [TestMethod]
        public void TestUnexistingFolder()
        {
            string path = "";
            
            FileTreeAnalyzer analyzer = new FileTreeAnalyzer(path);
            Assert.ThrowsException<ArgumentException>(analyzer.Retrieve);
            Assert.ThrowsException<InvalidOperationException>(analyzer.GetTree);
            Assert.ThrowsException<InvalidOperationException>(analyzer.GetExtensionsStats);
        }

        [TestMethod]

        public void TestExistingFolder()
        {
            string path = "C:\\TestFolder";
            FileTreeAnalyzer analyzer = new FileTreeAnalyzer(path);
            Assert.ThrowsException<InvalidOperationException>(analyzer.GetTree);
            Assert.ThrowsException<InvalidOperationException>(analyzer.GetExtensionsStats);
            analyzer.Retrieve();
            Assert.IsNotNull(analyzer.GetTree());
            Assert.IsNotNull(analyzer.GetExtensionsStats());
            Assert.AreEqual(analyzer.GetTree().GetSize(), 78401682);
            Assert.AreEqual(analyzer.GetExtensionsStats().Keys.Count, 4);
            Assert.AreEqual(analyzer.GetExtensionsStats()["folders"], 2);
            Assert.AreEqual(analyzer.GetExtensionsStats()["files"], 6);
        }

        [TestMethod]

        public void TestUnavailableFolder()
        {
            string path = "C:\\Documents And Settings";
            FileTreeAnalyzer analyzer = new FileTreeAnalyzer(path);
            Assert.ThrowsException<InvalidOperationException>(analyzer.GetTree);
            Assert.ThrowsException<InvalidOperationException>(analyzer.GetExtensionsStats);
            analyzer.Retrieve();
            Assert.IsNotNull(analyzer.GetTree());
            Assert.IsNotNull(analyzer.GetExtensionsStats());
            Assert.AreEqual(analyzer.GetTree().GetSize(), 0);
            Assert.AreEqual(analyzer.GetExtensionsStats().Keys.Count, 0);
        }

        [TestMethod]

        public void TestFileInsteadOfFolder()
        {
            string path = "C:\\TestFolder\\Marshmello - Alone [Monstercat Official Music Video].mp4";
            FileTreeAnalyzer analyzer = new FileTreeAnalyzer(path);
            Assert.ThrowsException<InvalidOperationException>(analyzer.GetTree);
            Assert.ThrowsException<InvalidOperationException>(analyzer.GetExtensionsStats);
            Assert.ThrowsException<ArgumentException>(analyzer.Retrieve);
            Assert.ThrowsException<InvalidOperationException>(analyzer.GetTree);
            Assert.ThrowsException<InvalidOperationException>(analyzer.GetExtensionsStats);
        }

        [TestMethod]

        public void TestInvalidArguments()
        {
            Assert.ThrowsException<ArgumentNullException>(()=>new FileTreeAnalyzer(null));
            Assert.ThrowsException<ArgumentOutOfRangeException>(()=>new FileTreeAnalyzer("", -1));
            Assert.IsNotNull(new FileTreeAnalyzer("", 0));
        }
    }
}
