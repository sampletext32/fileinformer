﻿using System;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;

namespace FileInformer
{
    public partial class FormMain : Form
    {
        private FileTreeAnalyzer m_analyzer;

        public FormMain()
        {
            InitializeComponent();

            LogBuilder.Get().OnAppend += (logLine) =>
            {
                if (listBoxLog.InvokeRequired)
                {
                    listBoxLog.Invoke(new Action(() => { listBoxLog.Items.Add(logLine); }));
                }
                else
                {
                    listBoxLog.Items.Add(logLine);
                }
            };
            LogBuilder.Get().OnClear += () =>
            {
                if (listBoxLog.InvokeRequired)
                {
                    listBoxLog.Invoke(new Action(() => { listBoxLog.Items.Clear(); }));
                }
                else
                {
                    listBoxLog.Items.Clear();
                }
            };
        }


        private void buttonSelectRootFolder_Click(object sender, EventArgs e)
        {
            using (var fbd = new FolderBrowserDialog())
            {
                DialogResult result = fbd.ShowDialog();

                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                {
                    m_analyzer = new FileTreeAnalyzer(fbd.SelectedPath);
                }
            }
        }

        private void buttonAnalyze_Click(object sender, EventArgs e)
        {
            if (m_analyzer == null)
            {
                MessageBox.Show("You should first specify folder");
                return;
            }
            m_analyzer.SetNestingLevel(numericUpDownMaxNestingLevel.Value == 0
                ? int.MaxValue
                : (int) numericUpDownMaxNestingLevel.Value);
            m_analyzer.Retrieve();
        }

        private void buttonPrepareHtml_Click(object sender, EventArgs e)
        {
            if (m_analyzer == null || !m_analyzer.IsTreeAvailable())
            {
                MessageBox.Show("You should first analyze folder");
                return;
            }
            string totalHtml =
                @"<!DOCTYPE html>" +
                @"<html>" +
                @"   <head>" +
                "      <meta charset=\"utf - 8\" />" +
                @"        <title>FileInformer Report</title>" +
                "        <style type=\"text/css\">  ul {border-left: 1px solid black;border-bottom: 1px solid black; margin:0; margin-left:10px; padding:0; padding-bottom:5px;} li {list-style-type:none;margin:0; margin-left:10px;} </style>" +
                @"     " +
                @"        </head>" +
                @"     " +
                @"        <body>" +
                @"     " +
                $"<ul>{m_analyzer?.GetTree().WrapHtml()}</ul>" +
                (checkBoxIncludeExtensionsStats.Checked
                    ? "<h3>Extensions Stats<h3>" +
                      PrepareExtensionStats()
                    : "") +
                @"           </body>" +
                @"        </html>";
            File.WriteAllText("page.html", totalHtml);
            Process.Start("page.html");
        }

        private string PrepareExtensionStats()
        {
            return "<ul><li>" + string.Join("</li><li>", m_analyzer.GetExtensionsStats()) + "</li></ul>";
        }
    }
}