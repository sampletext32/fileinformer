﻿namespace FileInformer
{
    partial class FormMain
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.buttonSelectRootFolder = new System.Windows.Forms.Button();
            this.buttonAnalyze = new System.Windows.Forms.Button();
            this.buttonPrepareHtml = new System.Windows.Forms.Button();
            this.listBoxLog = new System.Windows.Forms.ListBox();
            this.numericUpDownMaxNestingLevel = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.checkBoxIncludeExtensionsStats = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMaxNestingLevel)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(8, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(94, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "FileInformer";
            // 
            // buttonSelectRootFolder
            // 
            this.buttonSelectRootFolder.Location = new System.Drawing.Point(8, 40);
            this.buttonSelectRootFolder.Name = "buttonSelectRootFolder";
            this.buttonSelectRootFolder.Size = new System.Drawing.Size(136, 32);
            this.buttonSelectRootFolder.TabIndex = 1;
            this.buttonSelectRootFolder.Text = "Select Root Folder";
            this.buttonSelectRootFolder.UseVisualStyleBackColor = true;
            this.buttonSelectRootFolder.Click += new System.EventHandler(this.buttonSelectRootFolder_Click);
            // 
            // buttonAnalyze
            // 
            this.buttonAnalyze.Location = new System.Drawing.Point(8, 80);
            this.buttonAnalyze.Name = "buttonAnalyze";
            this.buttonAnalyze.Size = new System.Drawing.Size(136, 32);
            this.buttonAnalyze.TabIndex = 2;
            this.buttonAnalyze.Text = "Analyze";
            this.buttonAnalyze.UseVisualStyleBackColor = true;
            this.buttonAnalyze.Click += new System.EventHandler(this.buttonAnalyze_Click);
            // 
            // buttonPrepareHtml
            // 
            this.buttonPrepareHtml.Location = new System.Drawing.Point(8, 384);
            this.buttonPrepareHtml.Name = "buttonPrepareHtml";
            this.buttonPrepareHtml.Size = new System.Drawing.Size(136, 32);
            this.buttonPrepareHtml.TabIndex = 3;
            this.buttonPrepareHtml.Text = "Prepare HTML";
            this.buttonPrepareHtml.UseVisualStyleBackColor = true;
            this.buttonPrepareHtml.Click += new System.EventHandler(this.buttonPrepareHtml_Click);
            // 
            // listBoxLog
            // 
            this.listBoxLog.FormattingEnabled = true;
            this.listBoxLog.Location = new System.Drawing.Point(152, 40);
            this.listBoxLog.Name = "listBoxLog";
            this.listBoxLog.Size = new System.Drawing.Size(640, 381);
            this.listBoxLog.TabIndex = 4;
            // 
            // numericUpDownMaxNestingLevel
            // 
            this.numericUpDownMaxNestingLevel.Location = new System.Drawing.Point(8, 160);
            this.numericUpDownMaxNestingLevel.Name = "numericUpDownMaxNestingLevel";
            this.numericUpDownMaxNestingLevel.Size = new System.Drawing.Size(136, 20);
            this.numericUpDownMaxNestingLevel.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 120);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(95, 39);
            this.label2.TabIndex = 6;
            this.label2.Text = "Max Nesting Level\r\n0 - unlimited\r\n1 - current folder";
            // 
            // checkBoxIncludeExtensionsStats
            // 
            this.checkBoxIncludeExtensionsStats.AutoSize = true;
            this.checkBoxIncludeExtensionsStats.Location = new System.Drawing.Point(8, 192);
            this.checkBoxIncludeExtensionsStats.Name = "checkBoxIncludeExtensionsStats";
            this.checkBoxIncludeExtensionsStats.Size = new System.Drawing.Size(137, 17);
            this.checkBoxIncludeExtensionsStats.TabIndex = 7;
            this.checkBoxIncludeExtensionsStats.Text = "Include Extension Stats";
            this.checkBoxIncludeExtensionsStats.UseVisualStyleBackColor = true;
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.checkBoxIncludeExtensionsStats);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.numericUpDownMaxNestingLevel);
            this.Controls.Add(this.listBoxLog);
            this.Controls.Add(this.buttonPrepareHtml);
            this.Controls.Add(this.buttonAnalyze);
            this.Controls.Add(this.buttonSelectRootFolder);
            this.Controls.Add(this.label1);
            this.Name = "FormMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form Main";
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMaxNestingLevel)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonSelectRootFolder;
        private System.Windows.Forms.Button buttonAnalyze;
        private System.Windows.Forms.Button buttonPrepareHtml;
        private System.Windows.Forms.ListBox listBoxLog;
        private System.Windows.Forms.NumericUpDown numericUpDownMaxNestingLevel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox checkBoxIncludeExtensionsStats;
    }
}

